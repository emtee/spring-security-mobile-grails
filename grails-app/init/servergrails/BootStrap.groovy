package servergrails

import org.test.mserver.model.Role
import org.test.mserver.model.User
import org.test.mserver.model.UserRole

class BootStrap {

    def init = { servletContext ->

        def roleUser = new Role('ROLE_USER').save()
        def user = new User("user", "user").save()
        UserRole.create user, roleUser
    }
    def destroy = {
    }
}
