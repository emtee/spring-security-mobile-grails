package org.test.mserver

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import org.test.mserver.bean.Dummy

class IndexController {

    def springSecurityService

    @Secured("ROLE_ANONYMOUS")
    def insecure() {
        render new Dummy(msg: "hi there") as JSON
    }

    @Secured("ROLE_USER")
    def secure() {
        render new Dummy(msg: "hi there, ${springSecurityService.currentUser.username}" ) as JSON
    }
}

